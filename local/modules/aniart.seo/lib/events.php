<?php

namespace Aniart\Seo;

use \Bitrix\Main\Loader,
    \Aniart\Seo\SmartSeo as SmartSeo;

class Events
{
    static public function onEpilog()
    {
        $uriString  = \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getRequestUri();
        $isAdminPanel = preg_match("|^/bitrix/.*$|", $uriString) ? true : false;

        if (Loader::includeModule('aniart.main') && ! $isAdminPanel) {
            $smartSeo = SmartSeo::getInstance();
            $smartSeo->init();

            $seo = new SeoParamsCollector();
            $page = $smartSeo->isPageFound();
            if ($page) {
                $objHighload = new Highload();
                $seoData = $objHighload->getData($page);

                $seo->setPageTitle($seoData['UF_PAGE_TITLE'], true);
                $seo->setMetaTitle($seoData['UF_META_TITLE'], true);
                $seo->setKeywords($seoData['UF_KEYWORDS'], true);
                $seo->setDescription($seoData['UF_DESCRIPTION'], true);
            }
            $seo->process();
        }
    }
}
