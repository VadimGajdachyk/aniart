<?php

namespace Aniart\Seo;

use \Bitrix\Main\Loader,
    Bitrix\Highloadblock\HighloadBlockTable,
    Bitrix\Main\SystemException,
    CUserTypeEntity;

class Highload
{
    public $headers = array(),
        $tableName = 'seo_meta_data',
        $highloadName = 'Seometadata',
        $highloadTableId = 0;

    public function __construct()
    {
        if (!Loader::includeModule('highloadblock')) {
            throw new SystemException('Module does not include');
        } else {
            $this->headers = array(
                'PAGE',
                'PAGE_TITLE',
                'SORT',
                'META_TITLE',
                'KEYWORDS',
                'DESCRIPTION',
            );
        }
    }

    public function create()
    {
        if ($tableId = $this->issetTable()) {
            $this->highloadTableId = $tableId;
        } else {
            // create table
            $result = HighloadBlockTable::add(
                array(
                    'NAME' => $this->highloadName,
                    'TABLE_NAME' => $this->tableName,
                )
            );
            if ($result->isSuccess()) {
                $this->highloadTableId = $result->getId();
                $this->createFields();
            } else {
                $arErrors = $result->getErrorMessages();

                $message = '';
                if (is_array($arErrors)) {
                    $message .= implode("\n", $arErrors);
                } else {
                    $message .= $arErrors;
                }

                throw new SystemException($message);
            }
        }
    }

    /**
     * get table id if isset it
     *
     * @return int
     */
    public function issetTable()
    {
        $highloadTable = HighloadBlockTable::getList(
            array('filter' => array('NAME' => $this->highloadName, 'TABLE_NAME' => $this->tableName))
        );

        $tableId = 0;
        if ($arHighloadTable = $highloadTable->Fetch()) {
            $tableId = $arHighloadTable['ID'];
        }

        return $tableId;
    }

    /**
     * create fields for table
     */
    public function createFields()
    {
        $userTypeEntity = new CUserTypeEntity();
        foreach ($this->headers as $name) {
            $userTypeData = array(
                'ENTITY_ID' => 'HLBLOCK_'.$this->highloadTableId,
                'FIELD_NAME' => 'UF_'.$name,
                'USER_TYPE_ID' => 'string', // didn't dynamic type field
                'EDIT_FORM_LABEL' =>
                    array(
                        'ru' => $name,
                        'en' => $name,
                    ),
                'LIST_COLUMN_LABEL' =>
                    array(
                        'ru' => $name,
                        'en' => $name,
                    ),
                'SETTINGS' => array(),
                'LIST_FILTER_LABEL' =>
                    array(
                        'ru' => $name,
                        'en' => $name,
                    ),
                'ERROR_MESSAGE' =>
                    array(
                        'ru' => 'Ошибка при работе с полем'.$name,
                        'en' => 'Error with work on field'.$name,
                    ),
                'HELP_MESSAGE' =>
                    array(
                        'ru' => '',
                        'en' => '',
                    )
            );

            $userTypeEntity->Add($userTypeData);
        }
    }

    public function deleteTable()
    {
        if ($tableId = $this->issetTable()) {
            if ($tableId) {
                HighloadBlockTable::delete($tableId);
            }
        }
    }

    public function getData($page)
    {
        if ($tableId = $this->issetTable()) {
            $highloadBlock = HighloadBlockTable::getById($tableId)->fetch();
            $entity = HighloadBlockTable::compileEntity($highloadBlock);
            $entity_class = $entity->getDataClass();

            $rsData = $entity_class::getList(
                array(
                    'filter' => array('UF_PAGE' => $page),
                    'select' => array(
                        'UF_PAGE',
                        'UF_PAGE_TITLE',
                        'UF_SORT',
                        'UF_META_TITLE',
                        'UF_KEYWORDS',
                        'UF_DESCRIPTION',
                    ),
                    'order' => array('ID' => 'ASC'),
                )
            );

            $seoData = array();
            if ($arResult = $rsData->Fetch()) {
                $seoData = $arResult;
            }

            return $seoData;
        }
    }
}
