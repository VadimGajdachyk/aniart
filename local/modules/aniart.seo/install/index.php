<?php

use Bitrix\Main\Localization\Loc,
    \Bitrix\Main\ModuleManager,
    \Bitrix\Main\Loader,
    \Bitrix\Main\EventManager;

Loc::loadMessages(__FILE__);

class aniart_seo extends CModule
{
    public function __construct()
    {
        $arModuleVersion = array();

        include __DIR__.'/version.php';

        if (is_array($arModuleVersion)
            && array_key_exists(
                'VERSION',
                $arModuleVersion
            )
        ) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_ID = 'aniart.seo';
        $this->MODULE_NAME = Loc::getMessage('SEO_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('SEO_MODULE_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('SEO_MODULE_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('SEO_MODULE_PARTNER_URI');
    }

    public function DoInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        $this->InstallDB();
        $this->InstallEvents();
    }

    public function DoUninstall()
    {
        $this->UnInstallDB();
        $this->UnInstallEvents();
        \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    public function InstallDB()
    {
        Loader::includeModule('aniart.seo'); // for include classes, because autoload is hard for bitrix
        $objHighload = new \Aniart\Seo\Highload();
        $objHighload->create();
    }

    public function UnInstallDB() {
        Loader::includeModule('aniart.seo'); // for include classes, because autoload is hard for bitrix
        $objHighload = new \Aniart\Seo\Highload();
        $objHighload->deleteTable();
    }

    public function InstallEvents() {
        $eventEntity = EventManager::getInstance();
        $eventEntity->registerEventHandler('main', 'OnEpilog', $this->MODULE_ID, '\Aniart\Seo\Events', 'onEpilog');
    }

    public function UnInstallEvents() {
        $eventManager = EventManager::getInstance();
        $eventManager->unRegisterEventHandler('main', 'OnEpilog', $this->MODULE_ID, '\Aniart\Seo\Events', 'onEpilog');
    }
}
