<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$arIblockTypes = array();
$rsIblockTypes = CIBlockType::GetList();
while ($ar = $rsIblockTypes->Fetch()) {
    $arIBType = CIBlockType::GetByIDLang($ar['ID'], LANG);
    $arIblockTypes[$ar['ID']] = '['.$ar['ID'].']'.$arIBType['NAME'];
}

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "PAGE_ELEMENT_COUNT" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage('PAGE_ELEMENT_COUNT'),
            "TYPE" => "STRING",
            "DEFAULT" => "2",
        ),
        'IBLOCK_TYPE' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('FIELD_IBLOCK_TYPE'),
            'TYPE' => 'LIST',
            'VALUES' => $arIblockTypes,
            'REFRESH' => 'Y',
        ),
        'IBLOCK_ID' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('IBLOCK_ID'),
            'TYPE' => 'LIST',
            'VALUES' => array('NULL' => GetMessage('IBLOCK_ID_EMPTY')),
        ),
    ),
);

if (strlen($arCurrentValues['IBLOCK_TYPE']) > 0) {
    $arIblock = array();
    $rsIblock = Ciblock::GetList(array(), array('ACTIVE' => 'Y', 'TYPE' => $arCurrentValues['IBLOCK_TYPE']));
    while ($ar = $rsIblock->Fetch()) {
        $arIblock[$ar['ID']] = '['.$ar['ID'].']'.$ar['NAME'];
    }
    $arComponentParameters['PARAMETERS']['IBLOCK_ID']['VALUES'] = $arIblock;
}
