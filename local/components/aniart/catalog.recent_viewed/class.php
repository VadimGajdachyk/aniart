<?php

use Bitrix\Iblock,
    CCatalogProduct;

class CCatalogRecentViewed extends CBitrixComponent
{
    public function __construct($component = null)
    {
        parent::__construct($component);
    }

    private function checkParams()
    {
        if (!IntVal($this->arParams['PAGE_ELEMENT_COUNT'])) {
            $this->arParams['PAGE_ELEMENT_COUNT'] = 5;
        }
    }

    public function executeComponent()
    {
        $this->includeComponentLang('class.php');

        $arViewed = array();
        $basketUserId = (int) CSaleBasket::GetBasketUserID(false);
        if ($basketUserId > 0) {
            $viewedIterator = Bitrix\Catalog\CatalogViewedProductTable::getList(
                array(
                    'select' => array('PRODUCT_ID', 'ELEMENT_ID'),
                    'filter' => array('=FUSER_ID' => $basketUserId, '=SITE_ID' => SITE_ID),
                    'order' => array('DATE_VISIT' => 'DESC'),
                    'limit' => $this->arParams['PAGE_ELEMENT_COUNT'],
                )
            );

            while ($arFields = $viewedIterator->fetch()) {
                $arViewed[] = $arFields['ELEMENT_ID'];
            }
        }

        if (!empty($arViewed)) {
            $arOrderEl = array('SORT' => 'ASC');
            $arFilterEl = array(
                'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
                'ID' => $arViewed,
            );
            $arSelectFieldsEl = array(
                'ID',
                'ACTIVE',
                'NAME',
                'IBLOCK_ID',
                'PREVIEW_PICTURE',
                'DETAIL_PICTURE',
                'DETAIL_PAGE_URL',
            );
            $rsElements = CIBlockElement::GetList($arOrderEl, $arFilterEl, false, false, $arSelectFieldsEl);

            $this->arResult = array();
            while ($arElement = $rsElements->GetNext()) {
                Bitrix\Iblock\Component\Tools::getFieldImageData(
                    $arElement,
                    array('PREVIEW_PICTURE', 'DETAIL_PICTURE'),
                    Iblock\Component\Tools::IPROPERTY_ENTITY_ELEMENT,
                    'IPROPERTY_VALUES'
                );

                $mxResult = CCatalogSKU::GetInfoByProductIBlock(
                    $this->arParams['IBLOCK_ID']
                );

                $productId = $arElement['ID'];
                if (is_array($mxResult)) {
                    $rsOffers = CIBlockElement::GetList(array("PRICE" => "ASC"), array(
                        'IBLOCK_ID' => $mxResult['IBLOCK_ID'],
                        'PROPERTY_' . $mxResult['SKU_PROPERTY_ID'] => $arElement["ID"]
                    ));
                    while ($arOffer = $rsOffers->GetNext()) {
                        $productId = $arOffer['ID'];

                        break;
                    }
                }
                $arElement['PRICES'] = $this->getPrices($productId);

                $this->arResult['ITEMS'][] = $arElement;
            }
        }


        $this->includeComponentTemplate();
    }

    /**
     * get prices for item. If SKU then get min price from SKU
     *
     * @param $productId
     *
     * @return mixed
     */
    public function getPrices($productId)
    {
        $arBasePrice = CPrice::GetBasePrice($productId);

        global $USER;
        $arDiscounts = CCatalogDiscount::GetDiscountByProduct($productId, $USER->GetUserGroupArray(), 'N');

        if(is_array($arDiscounts)) {
            $finalPrice = CCatalogProduct::CountPriceWithDiscount(
                $arBasePrice['PRICE'],
                $arBasePrice['CURRENCY'],
                $arDiscounts
            );
        }

        $prices['BASE'] = array(
            'CAN_ACCESS' => 'Y',
            'DISCOUNT_VALUE' => $finalPrice,
            'PRINT_DISCOUNT_VALUE' => CurrencyFormat(
                $finalPrice,
                $arBasePrice['CURRENCY']
            ),
            'VALUE' => $arBasePrice['PRICE'],
            'PRINT_VALUE' => CurrencyFormat(
                $arBasePrice['PRICE'],
                $arBasePrice['CURRENCY']
            ),
        );

        return $prices;
    }
}
