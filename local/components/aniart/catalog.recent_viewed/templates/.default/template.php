<?php
use Aniart\Main\Interfaces\ProductInterface;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult['ITEMS'])) {
    return;
}
?>
<div id="recent_viewed_items" class="bx_item_list_you_looked_horizontal col5 bx_green">
    <div class="bx_item_list_title">Последние просмотренные товары:</div>
    <div class="bx_item_list_section">
        <div class="bx_item_list_slide" style="height: auto;">
            <?php foreach ($arResult['ITEMS'] as $arItem): ?>
                <div class="bx_catalog_item" style="position: relative" data-id="<?= $arItem['ID'] ?>">
                    <div class="">
                        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
                           class="bx_catalog_item_images"
                           style="background-image: url('<?= $arItem['DETAIL_PICTURE']['SRC'] ?>')"
                           title="<?= $arItem['NAME'] ?>">
                        </a>
                        <div class="bx_catalog_item_title">
                            <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" title="<?= $arItem['NAME'] ?>">
                                <?= $arItem['NAME'] ?>
                            </a>
                        </div>
                        <div class="bx_catalog_item_price">
                            <div class="bx_price">
                                <?= $arItem['PRICES']['BASE']['PRINT_DISCOUNT_VALUE'] ?>
                            </div>
                        </div>
                    </div>
                    <div class="delete-recent-item"></div>
                </div>
            <?php endforeach; ?>
            <div style="clear: both;"></div>
        </div>
    </div>
</div>
