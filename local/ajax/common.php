<?php require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php'); ?>

<?php
$productId = $_REQUEST['productId'];
if (!empty($productId)) {
    $arViewed = array();
    $basketUserId = (int) CSaleBasket::GetBasketUserID(false);
    if ($basketUserId > 0) {
        $viewedIterator = Bitrix\Catalog\CatalogViewedProductTable::getList(
            array(
                'select' => array('ID'),
                'filter' => array('=FUSER_ID' => $basketUserId, '=SITE_ID' => SITE_ID, 'ELEMENT_ID' => $productId),
                'order' => array('DATE_VISIT' => 'DESC'),
                'limit' => 1,
            )
        );

        if ($arFields = $viewedIterator->fetch()) {
            Bitrix\Catalog\CatalogViewedProductTable::delete($arFields['ID']);
        }
    }
}
$result = json_encode(
    array('STATUS' => 'OK')
);

die($result);
